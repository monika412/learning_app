import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from joblib import load, dump
from sklearn import linear_model
from flask import jsonify,make_response
from flask import jsonify
import json 

from flask import Flask
app = Flask(__name__)


result = ''

@app.route('/mlcase')
def get_prediction():
    with open('Demographic.csv', encoding='ISO-8859-1') as f:
        dataset = pd.read_csv(f, delimiter=',')

        dataset2 = dataset[['age','MonthlyIncome','NumberOfDependents','Gender']]
        load_clf1 = joblib.load('lm.pkl')
        print (load_clf1)
        X_total1 = dataset2.drop(['Gender'], axis=1)
        Y_pred1 = load_clf1.predict(X_total1)
        
        dtype = [('Col1','int32')]
        values = Y_pred1
        index = ['Row'+str(i) for i in range(1, len(values)+1)]
        
        prob_df2 = pd.DataFrame(values, index=index)
        prob_df2.reset_index(inplace=True,drop = True)
        dataset2['Pred_Income']=prob_df2[0]

        final_default_list = dataset2.drop_duplicates()
        print(final_default_list.head(10))
        
        Combine_Models = final_default_list[['Gender','Pred_Income']]
        final_surrogate = Combine_Models.drop_duplicates()
        
        dict_lst = []
        for i in final_surrogate.iterrows():
            
            Gender = i[1]['Gender']
            #Defaulter = i[1]['Default']
            Predicted_Income = i[1]['Pred_Income']

            elm = {"Gender": Gender,"Predicted_Income":Predicted_Income}
            dict_lst.append(elm)

        res_data_dict = {"Incomes":dict_lst}
        print(res_data_dict)
    
        response = app.response_class(response = json.dumps(res_data_dict),status = 200,mimetype='application/json')
        print('Response data',response)
        return (response)

if __name__ == '__main__':
    app.run(port=8080,host='0.0.0.0',debug=True)
