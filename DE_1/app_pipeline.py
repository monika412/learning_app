import pandas as pd
import numpy as np
import csv
import logging
from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib
from joblib import load, dump
from sklearn import linear_model

from flask import Flask
app = Flask(__name__)


filename1= 'lm.pkl'
#filename2= 'lm.pkl'

module_logger = logging.getLogger("exampleApp.app_pipeline")

#@app.route('/')

def LoadData():
    data = pd.read_csv("D:\python_examples\pipeline\Demographic.csv")
    return data
 
def Check_Cols(df):
	data_cols = ['age','MonthlyIncome','NumberOfDependents','Gender']
	data_cols_1 = df[data_cols]
	
	return data_cols_1.columns
	
def check_nulls(df):
	df.isnull().sum()
	logger = logging.getLogger("exampleApp.app_pipeline.check_nulls")
	logger.info("Nulls in data are " % (df.isnull().sum()))
	return(df)

def add_column(df):
	df['NumberOfDependents'].fillna(0,inplace=True)
	df['MonthlyIncome'].fillna(0,inplace=True)
	df['NumberOfDependents']=df['NumberOfDependents'].apply(np.int64)
	df['NumberOfDependents'] = [1 if x == 0 else x+1  for x in df['NumberOfDependents']]
	df['IncomePerPerson']=df['MonthlyIncome']/df['NumberOfDependents']
	df['IncomePerPerson']=df['IncomePerPerson'].astype(int)
	return (df)

def write_to_file(df):
	df_1=add_column(df)
	df_1.to_csv("result.csv")
	return True
	
def train_model(feature_data,filename1):
	clf=linear_model.LinearRegression()
	X_total = feature_data.drop(['Gender', 'MonthlyIncome'], axis=1)
	Y_total = feature_data['MonthlyIncome']
	clf.fit(X_total, Y_total)
	joblib.dump(clf,filename1)
	return (clf)
	
	
def PickleModel_LM(feature_data,clf):
	load_clf = joblib.load(filename1)
	X_total = feature_data.drop(['Gender', 'MonthlyIncome'], axis=1)
	Y_pred = load_clf.predict(X_total)
	values = Y_pred
	index = ['Row'+str(i) for i in range(1, len(values)+1)]
	prob_df1 = pd.DataFrame(values, index=index)
	prob_df1.reset_index(inplace=True,drop = True)
	feature_data['Pred_Income']=prob_df1[0]
	return (feature_data)

if __name__ == "__main__":
	ddf=LoadData()
	ddf1 = Check_Cols(ddf)
	ddf2 = add_column(ddf)
	clf=train_model(ddf2,filename1)
	df3=PickleModel_LM(ddf2,clf)
	print(df3)


	
