import unittest
import app_pipeline
import logging
import os
class test_app_pipeline(unittest.TestCase):
	file_name = "Demographic.csv"
	data = app_pipeline.LoadData()

		
	
	def test_LoadData_Count(self):
		columns_ct=len(self.__class__.data)
		self.assertTrue(len(self.__class__.data) >= 1, msg="The number of rows in the training data set is too less")
		
	
	def test_Columns_Count(self):
		datacolumns=[u'age', u'MonthlyIncome', u'NumberOfDependents',u'Gender']
		data = self.__class__.data 
		self.assertListEqual(list(data.columns) ,datacolumns, msg="The columns does not match")
		
		
		
	def test_check_nulls(self):
		data= app_pipeline.check_nulls(self.__class__.data)
		nullcount = data.isnull().sum()
		self.assertEqual(nullcount.sum(), 0, "The dataset has null values")
		
	def test_add_column(self):
		data1= app_pipeline.add_column(self.__class__.data)
		chk_Income=data1.IncomePerPerson
		zero_Income =(chk_Income == 0).sum()
		#print (zero_Income)
		self.assertGreater(0,zero_Income,msg="Income cannot be zero")
	
####Integration test
	def test_file_created(self):
		data = self.__class__.data 
		file='result.csv'
		#df_1=app_pipeline.write_to_file(data)
		app_pipeline.write_to_file(data)
		#print (df_1)
		self.assertTrue(os.path.exists(file))
		#self.assertEqual(df_1,True,msg="Fail")
		
		
if __name__ == "__main__":
	logger = logging.getLogger("exampleApp")
	logger.setLevel(logging.INFO)
	fh = logging.FileHandler("test.log")
	formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
	fh.setFormatter(formatter)
	logger.addHandler(fh)
	logger.info("Program started")
	
	unittest.main()
	
