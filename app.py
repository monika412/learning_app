# Load libraries
import pandas
import os
#from pandas.plotting import scatter_matrix
#import matplotlib.pyplot as plt

from flask import Flask
app = Flask(__name__)

@app.route('/')
def get_prediction():
	# Load dataset
	
	
	dataset = pandas.read_csv("EscapingHydrocarbons.csv")
	df1 = dataset.head(5)
	return (df1.to_json())
	
if __name__ == '__main__':
	port = int(os.environ.get('PORT',5000))
	app.run(host='0.0.0.0', port=port)

	
	